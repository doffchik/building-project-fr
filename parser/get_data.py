
import fake_useragent
import json
import logging
import os
import random
import re
import requests
import mysql.connector


from bs4 import BeautifulSoup as bs
from openpyxl import load_workbook


STREET_REGEXP  = re.compile(r'вул\.?\s?(?P<street>[а-я]+)', re.I)


def get_html(url):
    user_agent = fake_useragent.UserAgent()
    user = user_agent.random
    headers = {'User-Agent': str(user)}
    r = requests.get(url, headers=headers, timeout=60)
    return r.text


def get_max_page(html):
    soup = bs(html, 'lxml')
    try:
        links = soup.find_all('span', class_="item fleft")[-1].text
        max_page = re.findall(r'\d+', links)
        return ''.join(max_page)
    except Exception:
        return str(2)


def get_all_links(link):
    html = get_html(link)
    link_list = []
    soup = bs(html, 'lxml')
    links = soup.find_all('a', class_="thumb")
    for link in links:
        ad_link = link.get('href')
        link_list.append(ad_link)
    return link_list


def get_all_pages(url, pages):
    pages_list = []
    for i in range(pages):
        new_url = f'{url}?page={str(i)}'
        pages_list.append(new_url)
    return pages_list


def get_number(url):
    user_agent = fake_useragent.UserAgent()
    user = user_agent.random
    headers = {'User-Agent': str(user)}
    response = requests.get(url, headers=headers, timeout=60)
    if response.status_code != 200:
        logging.error('%s code for %s', response.status_code, url)
        return

    html = response.text
    soup = bs(html, 'lxml')
    try:
        phone_exist = soup.find_all("strong", class_="xx-large")
        if len(phone_exist) != 0:
            try:
                token = re.findall("var phoneToken = '(.*?)'", soup.text)[0]
            except Exception:
                return
            id_ = re.findall(r'-ID(.*?)\.html', url)[0]
            number_url = 'https://www.olx.ua/uk/ajax/misc/contact/phone/{}/?pt={}'.format(id_, token)
            headers = {'referer':  str(url)}
            try:
                response = requests.get(number_url, cookies={'PHPSESSID': response.cookies['PHPSESSID']}, headers=headers, timeout=60)
            except Exception:
                return

            try:
                number = json.loads(response.text)['value']
            except KeyError:
                logging.error('No number found')
                return
            return number
        else:
            pass
    except Exception:
        pass


def format_number(number):
    try:
        number = re.findall(r"\d", number)[-9::]
        number = ''.join(number)
        return '+380{}'.format(number)
    except Exception:
        pass


def connect_db():
    try:
        connection = mysql.connector.connect(
            user='root', password='1606',
            host='127.0.0.1', database='real_estate_db',
            auth_plugin='mysql_native_password'
            )
        print("connected")
        return connection
    except mysql.connector.Error as error:
        print("Failed to insert record into parser table {}".format(error))


def insert_ad_data(connection, args, table_name):
    cursor = connection.cursor()
    query = """INSERT INTO """ + table_name + """(
                                                date_pub, city, region, district, street, title,
                                                title_slug, new_title, description, gen_desc, currency,
                                                price, adv_type, obj_type, building, photo_links, kitchen,
                                                rooms, floor, bathroom, walls, square, count_floors, heating,
                                                remont, planing, seller_id, phone, original_url, is_archival
                                                )""" \
            """VALUES(
                %(date_pub)s, %(city)s, %(region)s, %(district)s, %(street)s, %(title)s,
                %(title_slug)s, %(new_title)s, %(description)s, %(gen_desc)s, %(currency)s,
                %(price)s, %(adv_type)s, %(obj_type)s, %(building)s, %(photo_links)s, %(kitchen)s,
                %(rooms)s, %(floor)s, %(bathroom)s, %(walls)s, %(square)s, %(count_floors)s, %(heating)s,
                %(remont)s, %(planing)s, %(seller_id)s, %(phone)s, %(original_url)s, %(is_archival)s
                )"""
    cursor.execute(query, args)
    connection.commit()
    cursor.close()


def street_names():
    directory = os.path.dirname(__file__)

    print('------------------------')
    print(directory)
    print('------------------------')

    wb2 = load_workbook(f'./lviv.xlsx')
    worksheet1 = wb2['Select_Dictionary_With_Foreign_']
    list_st = []
    for i in range(2, 1287):
        item = worksheet1[f'H{i}'].value
        item2 = worksheet1[f'I{i}'].value
        if item:
            street = item
        else:
            street = item2
        list_st.append(street)
    return list_st


class Main():

    def __init__(self, html, street_names):
        self.soup = bs(html, 'lxml')
        self.names = street_names
        self.data_block = self.soup.find_all('span', class_="offer-details__name")

    @property
    def adv_type(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Виберіть рубрику":
                    result = i.parent
                    adv_type = result.find("strong").text
                    if re.search(r'Продаж', adv_type, re.IGNORECASE):
                        return "Продаж"
                    elif re.search(r'Оренда', adv_type, re.IGNORECASE):
                        return "Оренда"
        except Exception:
            return "-"

    @property
    def photo_links(self):
        try:
            photos = self.soup.find_all('ul', class_="descgallery__bucket")[0]
            links = re.findall(r'href="(.*)" ', str(photos))
            links = ', '.join(links)
            return links
        except ValueError:
            raise

    @property
    def price(self):
        try:
            price = self.soup.find_all('div', class_="pricelabel")[0].text.strip()
            result = re.findall(r'\d+', price)
            result = ''.join(result)
            return result
        except Exception:
            return "-"

    @property
    def curency(self):
        try:
            price = self.soup.find_all('div', class_="pricelabel")[0].text.strip()
            result = re.findall(r'\D+', price)
            result = ''.join(result).strip()
            return result[0]
        except Exception:
            return "-"

    @property
    def seller(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Оголошення від":
                    result = i.parent
                    result = result.find("strong").text
            return result
        except Exception:
            return "-"

    @property
    def fees(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Без комісії":
                    result = i.parent
                    result = result.find("strong").text
                    if result:
                        return result
                    else:
                        return "Є комісія"
        except Exception:
            return "-"

    @property
    def obj_type(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Тип об'єкта":
                    result = i.parent
                    self.result = result.find("strong").text
            return self.result
        except Exception:
            return "-"

    @property
    def remont(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Ремонт":
                    result = i.parent
                    remont = result.find("strong").text
            return remont
        except Exception:
            return "-"

    @property
    def building(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Тип будинку":
                    result = i.parent
                    building = result.find("strong").text
                    if building == 'Царський будинок':
                        building = 'Елітний будинок'
                    else:
                        building = building
            return building
        except Exception:
            return None

    @property
    def walls(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Тип стін":
                    result = i.parent
                    walls = result.find("strong").text
            return walls
        except Exception:
            return "-"

    @property
    def planing(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Планування":
                    result = i.parent
                    planing = result.find("strong").text
            return planing
        except Exception:
            return "-"

    @property
    def square(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Загальна площа":
                    result = i.parent
                    result = result.find("strong").text
            return result
        except Exception:
            return "-"

    @property
    def kitchen(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Площа кухні":
                    result = i.parent
                    result = result.find("strong").text
            return result
        except Exception:
            return "-"

    @property
    def bathroom(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Cанвузол":
                    result = i.parent
                    result = result.find("strong").text
            return result
        except Exception:
            return "-"

    @property
    def heating(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Опалення":
                    result = i.parent
                    square = result.find("strong").text
            return square
        except Exception:
            return "-"

    @property
    def rooms(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Кількість кімнат":
                    result = i.parent
                    rooms = result.find("strong").text
                    rooms = rooms.strip()
            return rooms
        except Exception:
            return None

    @property
    def building_hight(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Поверховість":
                    result = i.parent
                    square = result.find("strong").text
            return square
        except Exception:
            return "-"

    @property
    def floor(self):
        try:
            for i in self.data_block:
                value = i.text
                if value == "Поверх":
                    result = i.parent
                    floor = result.find("strong").text
            return floor
        except Exception:
            return "-"

    @property
    def description(self):
        try:
            descr = self.soup.find_all('div', id="textContent")[0].text
            self.descr = descr.strip()
            return self.descr
        except Exception:
            return None

    @property
    def mod_descr(self):
        word_list = ['ріелтор', 'ріелторам', 'ріелтори', 'ріелторів', 'ріелтором', 'ріелтору', 'ріелторські', 'ріелторських', 'ріелторським']
        makler_list = ['маклер', 'маклерам', 'маклери', 'маклерів', 'маклером', 'маклеру', 'маклерські', 'маклерських', 'маклерським']
        disturb_list = ['не турбувати', 'не турбуйте', 'не телефонувати', 'не працюю', 'не співпрацюю', 'не співпрацюєм', 'не співпрацюємо', ]
        names_list = ['Ігор']
        all_list = word_list + makler_list + disturb_list + names_list
        modified = self.descr
        for i in all_list:
            modified = re.sub(rf'\b{i}\b', '', modified, flags=re.IGNORECASE)
            modified = modified
        modified = re.sub(r'\bБез Комісії\b', '', modified, flags=re.IGNORECASE)
        modified = re.sub(r'\d+ - Показати номер -', '', modified)
        modified = re.sub(r'\n', ' ', modified)
        modified = re.sub(r'\s{4}|\s{3}|\s{2}', ' ', modified)
        modified = re.sub(r'\.{2}|\.\s+\.', '..', modified)
        modified = re.sub(r'\b(\.)\b', '. ', modified)
        return modified

    @property
    def street_1(self):
        try:
            streets = []
            for name in self.names:
                street = re.search(rf'\b{name}\b', self.description)
                if street:
                    street = street.group(0)
                    streets.append(street)
            street_1 = str(streets[0])
            return street_1
        except Exception:
            return None

    @property
    def street_3(self):
        try:
            streets = []
            for name in self.names:
                street = re.search(rf'\b{name}\b', self.title)
                if street:
                    street = street.group(0)
                    streets.append(street)
            street_2 = str(streets[0])
            return street_2
        except Exception:
            return None

    @property
    def street_2(self):
        street_mo = re.search(r'вул\.?\s?(?P<street>[а-я]+)', self.title, re.IGNORECASE)
        if street_mo is None:
            return None
        street = street_mo['street']
        if street in self.names:
            return street

    @property
    def street_extra(self):
        street_mo = re.findall(r'вул\.?\s?(\w+)', self.description, re.IGNORECASE)
        if street_mo:
            street = street_mo[0]
            street = street[:-3]
            for i in self.names:
                search_street = re.search(rf'{street}', str(i))
                if search_street:
                    found_street = i
                else:
                    pass
            return found_street
        else:
            return None

    @property
    def street_4(self):
        street_mo = re.search(r'вул\.?\s?(?P<street>[а-я]+)', self.description, re.IGNORECASE)
        if street_mo is None:
            return None
        street = street_mo['street']
        if street in self.names:
            return street

    @property
    def street(self):
        street_list = []
        if self.street_2:
            street_list.append(self.street_2)
        if self.street_extra:
            street_list.append(self.street_extra)
        if self.street_4:
            street_list.append(self.street_4)
        if len(street_list) == 1:
            street = street_list[0]
        elif len(street_list) > 1:
            street = str(set(street_list))
        else:
            street = None
        return street

    @property
    def title(self):
        title = self.soup.find_all('h1')[0].text.strip()
        return title

    @property
    def new_title(self):
        text = []
        if self.rooms and self.obj_type:
            part_1 = f'{self.rooms}-х кімнатна {self.obj_type.lower()}'
            text.append(part_1)
        else:
            if self.obj_type:
                part_1 = f'{self.obj_type.lower().capitalize()}'
        if self.square:
            text.append(self.square)
        if self.street:
            part_3 = f'вул.{self.street}'
            text.append(part_3)
        if self.building:
            text.append(self.building.lower())
        new_title = ", ".join(text)
        return new_title

    @property
    def city(self):
        try:
            location = self.soup.find_all('div', class_="offer-user__address")[0].text
            location = re.split(",", location)
            location = location[0].strip()
            return location
        except Exception:
            return "-"

    @property
    def region(self):
        try:
            location = self.soup.find_all('div', class_="offer-user__address")[0].text
            location = re.split(",", location)
            location = location[1].strip()
            location = re.split(" ", location)[0].strip()
            return location
        except Exception:
            return None

    @property
    def district(self):
        try:
            location = self.soup.find_all('div', class_="offer-user__address")[0].text
            location = re.split(",", location)
            location = location[2].strip()
            location = re.split(" ", location)[0].strip()
            return location
        except Exception:
            return None

    @property
    def name(self):
        try:
            name = self.soup.find_all('div', class_="offer-user__actions")[0].text.strip()
            name = re.split(' ', name)[0].strip()
            return name
        except Exception:
            return None

    @property
    def gen_desc(self):
        text = []
        street_list = ['по вулиці', 'по вул ', 'по вул.', 'вул.', 'вулиця', '']
        sell_list = ['Продається', 'Продаж!', 'Продаж', 'Продаж.', 'Продаж,', '']
        space_list = ['Загальна площа ', 'Загальною площею ', 'Площа ', '']
        descr_1 = f'{random.choice(sell_list)} {self.rooms}-кімнатна {self.obj_type.lower()}, {random.choice(street_list)} {self.street}, {self.district} район.'
        text.append(descr_1)
        if self.square != '-':
            descr_2 = f'{random.choice(space_list)}{self.square}.'
            descr_2.lstrip(' ')
            text.append(descr_2)
        if self.floor and self.building_hight != '-':
            if self.floor != '3':
                descr_4 = f'{self.floor}-ий поверх, {self.building_hight} поверхового будинку.'
                text.append(descr_4)
            else:
                descr_4 = f'{self.floor}-ій поверх, {self.building_hight} поверхового будинку.'
                text.append(descr_4)
        if self.walls != "-":
            house = []
            descr_5 = f'{self.walls} будинок.'
            house.append(descr_5)
            descr_5_1 = f'Будинок {self.walls.lower()}.'
            house.append(descr_5_1)
            text.append(random.choice(house))
        if self.heating != "-":
            heat = []
            descr_6 = f'{self.heating} опалення.'
            heat.append(descr_6)
            descr_6_1 = f'Опалення {self.heating.lower()}.'
            heat.append(descr_6_1)
            text.append(random.choice(heat))

        result = " ".join(text)
        return result


# # url = 'https://www.olx.ua/uk/obyavlenie/dizayn-proekt-parus-kuhnya-studya-spalnya-garderobna-lodzhya-IDJ9wL6.html'
# url = 'https://www.olx.ua/uk/obyavlenie/3km-vul-kostya-levitskogo-avstryskiy-paradniy-IDIKoYZ.html#1de7b80d81'
# main = Main(get_html(url), street_names())
# # print(street_names())
# print("Місто: " + main.city)
# print("Область: " + str(main.region))
# print("Район: " + str(main.district))
# print("Рубрика: " + str(main.adv_type))
# print("Оригінальна назва: " + main.title)
# print("Вулиця: " + main.street)
# print("Нова назва: " + main.new_title)

# # print('-----------------------------------------------------')
# print("Опис: " + main.description)
# # print('-----------------------------------------------------')
# # for i in range(10):
# print('-----------------------------------------------------')
# print("Генерований опис: " + main.gen_desc)
# print('-----------------------------------------------------')

# print(main.photo_links)
# print("Оголошення від: " + str(main.seller))
# print("Комісія: " + str(main.fees))
# print("Валюта: " + str(main.curency))
# print("Обєкт: " + str(main.obj_type))
# print("Вулиця: " + str(main.street))
# print("Загальна: " + str(main.square))
# print("Кухня: " + str(main.kitchen))
# print("Кімнат: " + str(main.rooms))
# print("Поверх: " + str(main.floor))
# print("К-сть поверхів: " + str(main.building_hight))
# print("Тип будинку: " + str(main.building))
# print("Опалення: " + str(main.heating))
# print("Санвузол: " + str(main.bathroom))
# print("Тип стін: " + str(main.walls))
# print("Ремонт: " + str(main.remont))
# print("Планування: " + str(main.planing))
# print("Ціна: " + str(main.price))
# print("Валюта: " + str(main.curency))

def main():

    connection = connect_db()
    table_name = 'Advertisement'

    str_names = street_names()

    main_links = ['https://www.olx.ua/uk/nedvizhimost/lvov/?search%5Bdistrict_id%5D=127']

    max_page = 1
    all_pages = []
    for link in main_links:
        pages = get_all_pages(link, max_page)
        all_pages = all_pages + pages

    all_links = []
    for page in all_pages:
        links = get_all_links(page)
        all_links = all_links + links

    for i in all_links:
        try:
            main = Main(get_html(i), str_names)
            city = main.city
            region = main.region
            district = main.district
            street = main.street
            title = main.title
            new_title = main.new_title
            desc = main.description
            gen_desc = main.gen_desc
            curency = main.curency
            price = main.price
            adv_type = main.adv_type
            building = main.building
            photo_links = main.photo_links
            kitchen = main.kitchen
            rooms = main.rooms
            floor = main.floor
            bathroom = main.bathroom
            walls = main.walls
            square = main.square
            count_floors = main.building_hight
            heating = main.heating
            remont = main.remont
            planing = main.planing
            seller = main.seller
            phone = get_number(i)
            args = {
                    'date_pub': None,
                    'city': city,
                    'region': region,
                    'district': district,
                    'street': street,
                    'title': title,
                    'title_slug': None,
                    'new_title': new_title,
                    'description': desc,
                    'gen_desc': gen_desc,
                    'currency': curency,
                    'price': price,
                    'adv_type': adv_type,
                    'obj_type': None,
                    'building': building,
                    'photo_links': photo_links,
                    'kitchen': kitchen,
                    'rooms': rooms,
                    'floor': floor,
                    'bathroom': bathroom,
                    'walls': walls,
                    'square': square,
                    'count_floors': None,
                    'heating': heating,
                    'remont': remont,
                    'planing': planing,
                    'seller_id': seller,
                    'phone': phone,
                    'original_url': i,
                    'is_archival': False
                    }
            insert_ad_data(connection, args, table_name)
        except Exception as e:
            print(e)
            pass

    if (connection.is_connected()):
        connection.close()
        print("MySQL connection is closed")


if __name__ == "__main__":
    main()
