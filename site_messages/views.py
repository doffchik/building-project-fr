import logging

from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from advertisement.models import Advertisement as Adv

from . import services

logger = logging.getLogger(__name__)


@login_required
def user_messages_list(request):

    if request.user.is_moderator:
        clients_ids = services.get_clients_ids_with_messages_for_moderator()
    else:
        ads_ids = services.get_replied_ads_ids_for_client(request.user)

    advertisement = Adv.objects.get(url_identifier=request.GET.get('adv')) if request.GET.get('adv') else None
    messages = services.get_all_messages(request, adv=advertisement)

    return render(request, 'messages/my_messages.html', context=locals())
