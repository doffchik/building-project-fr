from django.db import models
from django.utils import timezone
from django.shortcuts import reverse
from django.conf import settings

from advertisement.models import Advertisement as Adv

from user.models import User


class Message(models.Model):

    text = models.TextField()
    client = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name='client_message')

    is_admin_answer = models.BooleanField(default=False)
    adv = models.ForeignKey(Adv, on_delete=models.CASCADE, blank=True, null=True)
    date_creation = models.DateTimeField(auto_now_add=True)

    was_read_in = models.DateTimeField(null=True)

    def made_message_reviewed(self):
        """ method is us for changing date - was_reviewed_in field
        this field will be displayed in pages(like count of unread messages)
        and will be use for celery task to send notification to email about message"""
        self.was_read_in = timezone.now()
        self.save()
        return self.was_read_in

    def time_of_unread(self):
        return timezone.now() - self.date_creation if not self.was_read_in else False

    def is_read(self):
        return bool(self.was_read_in)

    def get_message_url_for_admin(self):
        return f"{settings.WEB_SITE_MAIN_URL}" \
               f"{reverse('advertisement:adv_detail', kwargs={'url_identifier': self.adv.url_identifier})}" \
               f"?client_id={self.client_id}" \
               f"#messages-block-for-moderator"

    @classmethod
    def get_messages(cls, client, adv):

        if client.is_moderator:
            return cls.objects.filter(adv=adv)
        else:
            return cls.objects.filter(client=client, adv=adv)

    class Meta:
        db_table = 'Message'

        get_latest_by = 'date_creation'
