from django.urls import re_path, path

from . import consumer

websocket_urlpatterns = [
    path('socket/adv/<str:url_identifier>/message_channel/', consumer.MessagesConsumer.as_asgi())
]
