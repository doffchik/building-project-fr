import os
import unittest
import logging

from django.test import TestCase
from django.conf import settings

from .models import Message
from user.models import User
from advertisement.models import Advertisement as Adv

logger = logging.getLogger(__name__)

ads_json_file = os.path.join(settings.BASE_DIR, 'serialized_ads', 'ads_May-27-2021(14:35:01).json')
count_test_ads = 2

test_moderator_message = 'moderator_message'
test_client_message = 'client_message'


class TestMessageList(TestCase):

    def test_page(self):
        response = self.client.get('/my-messages/')
        self.assertEqual(response.status_code, 200)

    def test_template_usage(self):
        response = self.client.get('/my-messages/')
        self.assertTemplateUsed(response, 'messages/my_messages.html')


class MessageTest(TestCase):

    def setUp(self) -> None:
        self.moderator = User.objects.get(username='moderator')  # created by post migrations signal
        self.user = User.objects.create(username='test', password='test')

        self.ads = Adv.create_from_file(file=ads_json_file, count=count_test_ads)

        self.adv = self.ads[0]

        self.client_message = Message.objects.create(
            text=test_client_message,
            client=self.user,
            adv=self.adv
        )
        self.message_moderator = Message.objects.create(
            text=test_moderator_message,
            client=self.user,
            adv=self.adv,
            is_admin_answer=True
        )

    def test_count_ads(self):
        self.assertEqual(len(self.ads), count_test_ads)

    def test_moderator_message(self):
        """Be careful text filed in Message model is not unique
        this is the base test for one message"""

        message = Message.objects.get(text=test_moderator_message)
        self.assertTrue(message)
        self.assertEqual(message.is_admin_answer, True)
        self.assertEqual(message.adv, self.adv)
        self.assertEqual(message.client, self.user)

    def test_client_message(self):
        """Be careful text filed in Message model is not unique
        this is the base test for one message"""

        message = Message.objects.get(text=test_client_message)

        self.assertTrue(message)
        self.assertEqual(message.is_admin_answer, False)
        self.assertEqual(message.adv, self.adv)
        self.assertEqual(message.client, self.user)
