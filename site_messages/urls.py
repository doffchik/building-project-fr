from django.urls import path
from . import views


urlpatterns = [
    path('my-messages/', views.user_messages_list, name='user_messages_list_url'),
]
