import logging

logger = logging.getLogger(__name__)

def is_authenticated(method):
    def wrapper(instance, *args, **kwargs):
        if not instance.user.is_authenticated:
            logger.critical('NOT IMPLEMENTED FOR ANONYMOUS USER')
            return None
        else:
            return method(instance, *args, **kwargs)
    return wrapper