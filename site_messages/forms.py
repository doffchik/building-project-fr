from django import forms
from .models import Message


class MessageForm(forms.ModelForm):

    class Meta:
        model = Message
        fields = ['text', 'client', 'adv']

        widgets = {
            'text': forms.TextInput(attrs={'class':'add-message-field'})
        }