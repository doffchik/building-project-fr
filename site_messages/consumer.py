import json
import logging

from django.core import serializers

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from .services import MessageManager
from advertisement.models import Advertisement as Adv
from . import tasks


logger = logging.getLogger(__name__)


class MessagesConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        self.adv_url_identifier = self.scope['url_route']['kwargs']['url_identifier']
        self.adv_name = f'adv_{self.adv_url_identifier}'

        await self.channel_layer.group_add(
            self.adv_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.adv_name,
            self.channel_name
        )

    async def receive(self, text_data):
        json_data = json.loads(text_data)
        text = json_data['text']
        client_id = json_data['client_id']
        adv_id = json_data['adv_id']
        is_from_moderator = json_data['is_from_moderator']

        new_message = await self.create_new_message(text, client_id, adv_id)
        data = {
            'text': new_message.text,
            'class': 'admin-message' if is_from_moderator == 'True' else 'client-message'
        }
        await self.channel_layer.group_send(
            self.adv_name,
            {
                'type': 'new_message',
                'message': data,
            }
        )
        tasks.send_message_to_email.apply_async(
            args=json.loads(serializers.serialize('json', [new_message, ])),
            countdown= 5
        )

    async def new_message(self, event):
        message = event['message']

        await self.send(
            text_data=json.dumps({
                'message': message
            })
        )

    @database_sync_to_async
    def create_new_message(self, text, client_id, adv_id):

        message = MessageManager(
            client_id=client_id,
            adv=Adv.objects.get(id=adv_id),
            user=self.scope['user']
        )

        message = message.send_messages(text)
        logger.info('Message was created and send')
        return message
