import logging
from abc import abstractmethod

from django.core.mail import send_mail
from django.utils import timezone
from django.conf import settings

from user.models import User

from .models import Message
from .decorators import is_authenticated

logger = logging.getLogger(__name__)


class BaseMessageManager:
    def __init__(self, adv, user):
        self.adv = adv
        self.user = user

    @abstractmethod
    def get_messages(self):
        pass

    @abstractmethod
    def send_messages(self):
        pass


class ClientMessageManager(BaseMessageManager):

    def get_messages(self):
        return Message.objects.filter(adv=self.adv, client=self.user)

    def send_message(self, text):
        message = Message.objects.create(
            adv=self.adv,
            client=self.user,
            text=text,
        )
        return message


class ModeratorMessageManager(BaseMessageManager):

    def __init__(self, client_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client_id = client_id

    def get_all_clients_id(self):
        ids = Message.objects.filter(adv=self.adv).values_list('client_id', flat=True).distinct()

        logger.info(ids)

        return [
            User.objects.get(id=id)
            for id in ids
        ]

    def get_messages(self):
        return Message.objects.filter(adv=self.adv, client_id=self.client_id)

    def send_message(self, text):
        return Message.objects.create(
            adv=self.adv,
            client_id=self.client_id,
            is_admin_answer=True,
            text=text
        )


class MessageManager(BaseMessageManager):

    def __init__(self, client_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client_id = client_id

    @is_authenticated
    def get_messages(self):
        if self.user.is_moderator:
            manager = ModeratorMessageManager(client_id=self.client_id, adv=self.adv, user=self.user)
            self.clients_id = manager.get_all_clients_id()
            return manager.get_messages()
        return ClientMessageManager(adv=self.adv, user=self.user).get_messages()

    @is_authenticated
    def send_messages(self, text):

        if self.user.is_moderator:
            return ModeratorMessageManager(adv=self.adv, user=self.user, client_id=self.client_id).send_message(text)
        return ClientMessageManager(adv=self.adv, user=self.user).send_message(text)


def check_type_of_message(message):
    return 'customer_message' if not message.is_admin_answer \
        else 'admin_message'


def send_to_admin_email(message_data: dict):

    message =  Message.objects.select_related(
        'adv', 'client').only('adv___url_identifier', 'client__id', 'client__username').get(id=message_data.get('pk'))
    url = message.get_message_url_for_admin()

    send_mail(
        f'New message - ["{message.client.username}"]',
        f"""You have new message from client
        You can use url to see it {url}""",
        None,
        [settings.EMAIL_HOST_USER],
        fail_silently = False
    )
    return True


def send_to_customer(message):
    logger.info('was send to customer')


def get_replied_ads_ids_for_client(user):
    messages_list = user.client_message.all().values_list('adv_id', 'adv__url_identifier').distinct()

    data = [
        list(message) + [Message.objects.filter(
            adv_id=message[0], client_id=user.id, is_admin_answer=True,
            was_read_in=None).count()]
        for message in messages_list
    ]
    return data


def get_clients_ids_with_messages_for_moderator():
    messages_list = Message.objects.values_list('client_id', 'adv___url_identifier').distinct()
    data = [
        list(message) + [Message.objects.filter(
            client_id=message[0], adv___url_identifier=message[1], is_admin_answer=False, was_read_in=None).count()]
        for message in messages_list
    ]
    return data



def get_all_messages(request, adv):

    if not adv:
        return None

    message_manager = MessageManager(request.GET.get('client_id'),
        adv=adv, user=request.user)

    messages = message_manager.get_messages()

    if messages and request.user.is_moderator:
        for message in messages.filter(was_read_in=None, is_admin_answer=False):
            message.was_read_in=timezone.now()
            message.save()
    elif messages:
        for message in messages.filter(was_read_in=None, is_admin_answer=True, client_id=request.user.id):
            message.was_read_in=timezone.now()
            message.save()

    return messages