import logging

from config.celery_config import app
from . import services
from .models import Message

logger = logging.getLogger(__name__)


@app.task(bind=True)
def send_message_to_email(self, new_message):

    def _inner():
        if new_message['fields']['is_admin_answer']:
            return services.send_to_customer('customer_message')
        else:
            return services.send_to_admin_email(new_message)

    if new_message['fields']['was_read_in'] is None:
        return _inner()
    else:
        return 'Was read'
