function hideButton(buttonId){
    button = document.getElementById(buttonId)
    button.hidden = true;
}

function changePhoneButton(buttonId, phoneNumber){
    button = document.getElementById(buttonId)
    button.textContent = phoneNumber
    button.id = 'get-phone-active'
}

function hideAdvBlock(id){
    element = document.getElementById(`adv-${id}`)
    element.style.display = 'none';
}

function setHiddenValueForButtons(arrButtonsId){
    buttons = document.getElementsByClassName('add-button')
    for (button of buttons){
        if (arrButtonsId.includes(button.value)){
            button.hidden = true;
        }
    }
    return buttons[0]
}