#!/bin/bash

source /venv/bin/activate

python manage.py makemigrations
python manage.py migrate
python manage.py set_mysql_charset
python manage.py loaddata data/db.json

export DJANGO_SETTINGS_MODULE=config.settings
gunicorn config.wsgi:application --bind 0.0.0.0:8080