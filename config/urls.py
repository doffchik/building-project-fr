from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static

# app_name = 'config'

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('accounts/', include('allauth.urls')),
    path('', include('core.urls')),
    path('', include('advertisement.urls')),
    path('', include('user.urls')),
    path('', include('filters.urls')),
    path('', include('auth_user.urls')),
    path('', include('site_messages.urls')),

]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
