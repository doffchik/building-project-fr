from pathlib import Path
import os

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'r%j^!vgopv$x!w(b*j(=xxz9b=dwn+(@zomt+*hhaq=_nhiuoo'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

WEB_SITE_MAIN_URL = 'http://localhost:8000'

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'advertisement',
    'core',
    'user.apps.UserConfig',
    'auth_user',
    'site_messages',
    'functional_test',

    'allauth',
    'allauth.account',
    # 'channels',

    'django_extensions',
    'mathfilters',
    'filters',
]

AUTH_USER_MODEL = 'user.User'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ALLOWED_HOSTS = ['*']

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [TEMPLATE_DIR,],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'
# ASGI_APPLICATION = "config.asgi.application"


CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG":{
            "hosts": [("redis", 6379)]
        }
    }
}

# Database

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'real_estate_db',
        'USER': 'root',
        'PASSWORD': '0602test',
        'HOST': 'mysql_db',
        'PORT': 3306,
        'OPTIONS': {
            'charset': 'utf8',
            'use_unicode':True,
        }
    }
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'


# Internationalization

LANGUAGE_CODE = 'uk-ua'

TIME_ZONE = 'UTC'
# TIME_ZONE = 'Etc/GMT-3'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# http://127.0.0.1:8000/
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
# STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static')
]

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

AUTH_PASSWORD_VALIDATORS = [

    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 2,
        }
    },
]

AUTHENTICATION_BACKENDS = [
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',
]

SITE_ID = 1

""" LOGGER CONFIGS """
LOG_DIR = os.path.join(BASE_DIR, 'logs')
if not os.path.exists(LOG_DIR):
    os.mkdir(LOG_DIR)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'main_formatter': {
            'format': '{levelname}| {asctime}| {module}| {message}',
            'style': '{'
        },
        'console_formatter': {
            '()': 'colorlog.ColoredFormatter',
            'format': '\n{log_color} {levelname}| {asctime}| {module}| {message}\n',
            'style': '{'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'console_formatter'
        },

        'error':{
            'level': 'ERROR',
            # 'filters': '',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'error.log'),
            'formatter': 'main_formatter',
        },
        'main_handler': {
            'level': 'DEBUG',
            # 'filters': '',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, 'main_logs.log'),
            'formatter': 'main_formatter'
        },

    },
    'loggers': {
        'core': {
            'handlers': ['error', 'main_handler', 'console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'user':{
            'handlers': ['error', 'main_handler', 'console'],
            'level':'INFO',
            'propagate': True,
        },
        'advertisement': {
            'handlers': ['error', 'main_handler', 'console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'filters': {
            'handlers': ['error', 'main_handler', 'console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'site_messages': {
            'handlers': ['error', 'main_handler', 'console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'functional_test': {
            'handlers': ['error', 'main_handler', 'console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'auth_user': {
            'handlers': ['error', 'main_handler', 'console'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}

""" ALLAUTH FORMS ROUTS """
ACCOUNT_FORMS = {
    'login': 'auth_user.forms.MainLoginForm',
    'signup': 'auth_user.forms.MainSignupForm',
    'add_email': 'allauth.account.forms.AddEmailForm',
    'change_password': 'allauth.account.forms.ChangePasswordForm',
    'set_password': 'allauth.account.forms.SetPasswordForm',
    'reset_password': 'allauth.account.forms.ResetPasswordForm',
    'reset_password_from_key': 'allauth.account.forms.ResetPasswordKeyForm',
    'disconnect': 'allauth.socialaccount.forms.DisconnectForm',
}

# after login/signup we should go to verification user account
LOGIN_REDIRECT_URL = '/accounts/verification/'

ACCOUNT_SIGNUP_PASSWORD_VERIFICATION = False
ACCOUNT_EMAIL_REQUIRED = False

""" EMAIL AND SMTP PROTOCOL """
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'real.estate.lviv.operator@gmail.com'
EMAIL_HOST_PASSWORD = 'test_pass'
EMAIL_PORT = 587

""" CELERY """
REDIS_HOST = 'redis'
REDIS_PORT = '6379'
CELERY_BROKER_URL = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/0'
CELERY_BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 3600}
CELERY_RESULT_BACKEND = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/0'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

try:
    from .local_settings import *
except ImportError:
    pass

if DEBUG is False:
    # SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
    # SECURE_SSL_REDIRECT = True
    # SESSION_COOKIE_SECURE = True
    # CSRF_COOKIE_SECURE = True
    pass
