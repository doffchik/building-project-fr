import logging
from django.core.management import BaseCommand
from django.db import connection

logger = logging.getLogger()


class Command(BaseCommand):

    def handle(self, *args, **options):
        with connection.cursor() as cursor:
            try:
                command_result = cursor.execute("""
                ALTER TABLE Advertisement CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
                ALTER TABLE Filters CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
                """)
                logging.info(command_result)
                logging.info('CHARSET WAS SET FOR TABLES')
            except Exception as error:
                logger.error('Something went wrong\n'
                             f'ERROR: {error}')

