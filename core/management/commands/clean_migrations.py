import os
import logging

from django.core.management import BaseCommand
from django.conf import settings

logger = logging.getLogger(__name__)


class Command(BaseCommand):


    def handle(self, *args, **options):
        answer = input('If you want to clean all migrations, please write "Yes": ')

        if answer.lower() == "yes":
            self.clean_migrations()
            logger.info('Migrations was cleaned')


    # Todo test the main path of django_project.
    def clean_migrations(self):
        django_project_folder = settings.BASE_DIR
        logger.info(f'Start cleaning migrations! folder: {django_project_folder}')

        dir_paths = [os.path.join(django_project_folder, path) for path in os.listdir(django_project_folder) if
                     os.path.isdir(path)]

        for path in dir_paths:
            if 'migrations' in os.listdir(path):
                self.delete_migrations_files(path_to_migrations=os.path.join(path, 'migrations'))


    def delete_migrations_files(self, path_to_migrations):
        migrations_files_in_app = os.listdir(path_to_migrations)

        for filename in migrations_files_in_app:
            migration_file = os.path.join(path_to_migrations, filename)

            if filename != '__init__.py':
                if os.path.exists(migration_file) and os.path.isfile(migration_file):
                    logger.info(f'Deleting file: {migration_file}')
                    os.remove(migration_file)
