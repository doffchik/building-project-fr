from django.apps import AppConfig


class BuildingConfig(AppConfig):
    name = 'core'
