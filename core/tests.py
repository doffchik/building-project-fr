import logging
from django.test import TestCase

logger = logging.getLogger(__name__)


class MainPageTest(TestCase):

    def test_redirect_in_home_page(self):
        """ test: if website is opened with location '/' it should redirect to '/ads/' """
        response = self.client.get('/')

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/ads/')

