from django.shortcuts import redirect, reverse


def main_redirector(view):
    def decorator(request):
        if 'moderators' in [str(group) for group in request.user.groups.all()]:
            return redirect(reverse('advertisement:adv_list_for_moderators'))
        return redirect(reverse('advertisement:adv_list'))
    return decorator


def is_moderator(view):
    """ Decorator for checking role"""

    def decorator(self, request, *args, **kwargs):
        if 'moderators' in [str(group) for group in request.user.groups.all()]:
            return view(self, request, *args, **kwargs)
        return redirect(reverse('core:base_redirector_url'))
    return decorator


def is_customer(view):
    def decorator(request, *args, **kwargs):
        if 'customers' in [str(group) for group in request.user.groups.all()]:
            return view(request, *args, **kwargs)
        return redirect(reverse('core:base_redirector_url'))
    return decorator
