from django.urls import path
from . import views

app_name = 'core'

urlpatterns = [
    path('', views.base_redirector, name='base_redirector_url'),
]
