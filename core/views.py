from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from .services import main_redirector


@main_redirector
def base_redirector(request):
    return HttpResponse(status=302)
