import logging

from django.test import TestCase
from django.shortcuts import reverse
from user.models import User

logger = logging.getLogger(__name__)


class UserTest(TestCase):

    def setUp(self) -> None:
        self.moderator = User.objects.create_moderator(username='moderator1', password='moderator1')

    def test_creating_moderators(self):
        self.assertEqual(self.moderator.is_moderator, True)
