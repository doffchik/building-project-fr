import logging

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views import View
from django.http import JsonResponse

from . import services
from advertisement import services as adv_services

logger = logging.getLogger(__name__)


class UserLikedAds(View):

    def get(self, request):
        user_advertisement = request.user.added_ads.all()
        return render(request, 'user/UserAdvertisement.html', {'user_adv':user_advertisement})


class HiddenAds(adv_services.AdvListMixin):
    search_by_query = True
    template_name = 'user/hidden_ads.html'

    def set_query(self, request):
        self.query = request.user.hidden_ads.all()


class ViewedAds(adv_services.AdvListMixin):
    search_by_query = True
    template_name = 'user/viewed_ads.html'

    def set_query(self, request):
        self.query = request.user.all_reviewed_ads()


def add_adv_ajax(request):
    user_adv = services.UserAdv(adv_id=request.POST.get('adv_id'), user=request.user)
    user_adv.add_adv_for_user()
    return JsonResponse({'button_id':request.POST.get('button_id')}, status=200)


def hide_adv(request, adv_id):
    user_adv = services.UserAdv(adv_id=adv_id, user=request.user)
    user_adv.hide_adv_for_user()
    return HttpResponseRedirect(request.GET.get('end_point'))


class Profile(View):
    def get(self, request):
        return render(request, 'user/Profile.html')
