import logging

from advertisement.models import Advertisement
from django.core.exceptions import ObjectDoesNotExist

logger = logging.getLogger(__name__)


class UserAdv:
    def __init__(self, user, adv_id=None, adv=None):
        if not adv:
            try:
                self.adv = Advertisement.objects.get(id=adv_id)
            except Advertisement.DoesNotExist:
                raise ObjectDoesNotExist('Something went wrong. \n This advertisement is not exist')
        else:
            self.adv = adv

        self.user = user

    def add_adv_for_user(self):
        self.user.added_ads.add(self.adv)

    def hide_adv_for_user(self):
        self.user.hide_adv(self.adv)

    def add_reviewed_adv_for_user(self):
        if self.user.is_authenticated:
            self.user.add_reviewed_adv(adv=self.adv)


def create_group(group_name):
    from django.contrib.auth.models import Group
    from django.contrib.auth.models import Permission
    from django.contrib.contenttypes.models import ContentType

    try:
        group = Group.objects.get(name=group_name)
        logger.debug(f'Group: {group_name} exists')
        return group

    except Group.DoesNotExist:
        adv_content = ContentType.objects.get(app_label='advertisement', model='advertisement')
        permissions = Permission.objects.filter(content_type_id=adv_content.id)

        group = Group(name=group_name)
        group.save()
        [group.permissions.add(permission) for permission in permissions]
        logger.debug(f'Group: "{group}" was created')
        return group
