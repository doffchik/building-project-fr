import logging

from django.apps import AppConfig
from django.db.models.signals import post_migrate

logger = logging.getLogger(__name__)

GROUPS = 'moderators',
SUPER_USER_NAME = 'admin'
SUPER_USER_PASSWORD = 'admin'

MODERATOR_NAME = 'moderator'
MODERATOR_PASSWORD = 'moderator'

TESTER_BOT_NAME = 'bot_tester'
TESTER_BOT_PASSWORD = 'bot_tester'


class UserConfig(AppConfig):
    name = 'user'

    def ready(self):
        post_migrate.connect(create_groups, sender=self)
        post_migrate.connect(create_super_user, sender=self)
        post_migrate.connect(create_user_moderator, sender=self)
        post_migrate.connect(create_tester_bot, sender=self)


def create_groups(sender, **kwargs):
    from .services import create_group

    for group in GROUPS:
        create_group(group)


def create_tester_bot(sender, **kwargs):
    from .models import User

    try:
        User.objects.get(username=TESTER_BOT_NAME)
    except User.DoesNotExist:
        User.objects.create_user(username=TESTER_BOT_NAME, password=TESTER_BOT_PASSWORD)


def create_super_user(sender, **kwargs):
    from .models import User

    try:
        User.objects.get(username=SUPER_USER_NAME)
        logger.debug(f'Super user exists')
    except User.DoesNotExist:
        User.objects.create_superuser(username=SUPER_USER_NAME, email='', password=SUPER_USER_PASSWORD)
        logger.debug(f'Super: "{SUPER_USER_NAME}" user was successfully created')


def create_user_moderator(sender, **kwargs):
    from .models import User

    if User.objects.filter(username=MODERATOR_NAME):
        logger.debug('Moderator exist')
    else:
        User.objects.create_moderator(
            username=MODERATOR_NAME,
            password=MODERATOR_PASSWORD
        )

        logger.debug('Moderator was successfully created!')