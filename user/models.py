import logging

from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.auth.models import AbstractUser, UserManager, Group
from django.db import models

from advertisement.models import Advertisement
from filters.models import Filter

logger = logging.getLogger(__name__)


class MainUserManager(UserManager):

    def create_moderator(self, username, password):
        user = self.create_user(username=username, password=password)
        group = Group.objects.get(name='moderators')

        user.groups.add(group)
        return user


class User(AbstractUser):
    """Custom User model"""

    objects = MainUserManager()

    _phone = PhoneNumberField(unique=True, null=True, db_column='phone')

    added_ads = models.ManyToManyField(Advertisement, default=None, blank=True, related_name='users_which_add')  #change related name
    hidden_ads = models.ManyToManyField(Advertisement, default=None, blank=True, related_name='users_which_hide')
    viewed_ads = models.ManyToManyField(Advertisement, default=None, blank=True, related_name='users_which_viewed')

    phone = property(lambda self: self._phone.as_e164)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    def __str__(self):
        return self.username

    @property
    def is_customer(self):
        if 'moderators' not in [str(group) for group in self.groups.all()]:
            return True
        return False

    @property
    def is_moderator(self):
        if 'moderators' in [str(group) for group in self.groups.all()]:
            return True
        return False

    @property
    def status(self):
        if self.is_moderator:
            return 'Moderator'
        elif self.is_customer:
            return 'Customer'

    def hide_adv(self, adv):
        self.hidden_ads.add(adv)

    def add_reviewed_adv(self, adv):
        self.viewed_ads.add(adv)

    def all_reviewed_ads(self):
        return self.viewed_ads.all()

    class Meta:
        db_table = 'User'