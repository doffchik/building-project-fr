from django.urls import path
from . import views

app_name = 'user'

urlpatterns = [
    path('profile/', views.Profile.as_view(), name='profile'),
    path('likedAds/', views.UserLikedAds.as_view(), name='user_ads'),
    path('add-adv-ajax/', views.add_adv_ajax, name='add_adv_ajax'),
    path('hide-adv/<int:adv_id>', views.hide_adv, name='hide_adv'),
    path('hidden-ads/', views.HiddenAds.as_view(), name='hidden_ads'),
    path('viewed-ads/', views.ViewedAds.as_view(), name='viewed_ads'),
]
