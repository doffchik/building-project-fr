from viberbot import Api
from viberbot.api.bot_configuration import BotConfiguration

from viberbot.api.viber_requests import ViberConversationStartedRequest
from viberbot.api.viber_requests import ViberFailedRequest
from viberbot.api.viber_requests import ViberMessageRequest
from viberbot.api.viber_requests import ViberSubscribedRequest
from viberbot.api.viber_requests import ViberUnsubscribedRequest

from viberbot.api.messages import VideoMessage
from viberbot.api.messages.text_message import TextMessage

viber = Api(BotConfiguration(
    name='Renta-Lviv-Viber',
    avatar='https://www.google.com/url?sa=i&url=https%3A%2F%2Fru.365psd.com%2Fistock%2Favatar-icon-of-girl-in-a-baseball-cap-1018255&psig=AOvVaw1ZDdYUT_pyBc2sbX1C04z0&ust=1624715772007000&source=images&cd=vfe&ved=0CAoQjRxqFwoTCLj8sYX4svECFQAAAAAdAAAAABAI',
    auth_token='4d71c75d1b67dd2e-c00404909caacada-24706530cde7be4'
))