import logging
from phonenumber_field.formfields import PhoneNumberField

from allauth.account.forms import LoginForm
from allauth.account.forms import SignupForm
from django import forms

logger = logging.getLogger(__name__)


class MainLoginForm(LoginForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        set_fields_class_name(self)


class MainSignupForm(SignupForm):

    email = forms.CharField(widget = forms.HiddenInput(), required = False)
    phone = PhoneNumberField(region='UA', widget=forms.TextInput(attrs={'placeholder': 'Телефон'}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        set_fields_class_name(self)
    
    def save(self, request):
        user = super(MainSignupForm, self).save(request)
        user._phone = self.cleaned_data['phone']
        user.save()
        return user

def set_fields_class_name(instance):
    for key, value in instance.fields.items():
        instance.fields[key].widget.attrs['class'] = 'form-control'


"""       
    'add_email': 'allauth.account.forms.AddEmailForm',
    'change_password': 'allauth.account.forms.ChangePasswordForm',
    'set_password': 'allauth.account.forms.SetPasswordForm',
    'reset_password': 'allauth.account.forms.ResetPasswordForm',
    'reset_password_from_key': 'allauth.account.forms.ResetPasswordKeyForm',
    'disconnect': 'allauth.socialaccount.forms.DisconnectForm',
"""