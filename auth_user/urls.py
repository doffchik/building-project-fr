from django.urls import path, include
from . import views

urlpatterns = [
    path('accounts/verification/', views.verification_page, name='verification_page'),
    path('viber/bot', views.viber_test, name='viber_test'),
    path('viber/send/test', views.test_viber_request, name='viber_test'),
]