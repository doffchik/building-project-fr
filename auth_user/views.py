import logging
from django.http import HttpResponse
from django.shortcuts import render

from .viber_app import *

logger = logging.getLogger(__name__)


def verification_page(request):
    return render(request, 'account/verification.html')


def viber_test(request):
    logger.info("received request")
    logger.info(request)
    # handle the request here
    if not viber.verify_signature(request.POST, request.headers.get('X-Viber-Content-Signature')):
        return HttpResponse(status=403)

    viber_request = viber.parse_request(request.POST)

    if isinstance(viber_request, ViberMessageRequest):
        message = viber_request.message
        # lets echo back
        viber.send_messages(viber_request.sender.id, [
            message
        ])
    elif isinstance(viber_request, ViberSubscribedRequest):
        viber.send_messages(viber_request.get_user.id, [
            TextMessage(text="thanks for subscribing!")
        ])
    elif isinstance(viber_request, ViberFailedRequest):
        logger.warning("client failed receiving message.")

    return HttpResponse(status=200)


def test_viber_request(request):
    response = viber.set_webhook('https://renta.lviv.ua/viber/bot')
    return response