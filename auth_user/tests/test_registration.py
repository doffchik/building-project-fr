import logging
from unittest import skip

from django.test import TestCase, Client
from django.forms.widgets import HiddenInput
from auth_user.forms import MainSignupForm
from user.models import User

logger = logging.getLogger(__name__)


class RegisterPageTest(TestCase):

    def test_template_usage(self):
        response = self.client.get('/accounts/signup/')
        self.assertTemplateUsed(response, 'account/signup.html')

    def test_redirect_after_register(self):
        response = self.client.post('/accounts/signup/', data={
            'password1': 'bot_tester_register_form',
            'username': 'bot_tester_register_form',
            'phone': '380966545795',
        })
        self.assertEqual(response.status_code, 302)
        self.assertIn('/accounts/verification/', response.url)

    def test_user_after_register(self):
        response = self.client.post('/accounts/signup/', data={
            'password1': 'bot_tester_register_form',
            'phone': '380966545888',
            'username': 'bot_tester_register_form',
        })

        created_user = User.objects.latest('id')
        self.assertEqual(created_user.username, 'bot_tester_register_form')
        self.assertEqual(created_user.phone, '+380966545888')


class VerificationPageTest(TestCase):

    def test_if_page_opens(self):
        response = self.client.get('/accounts/verification/')
        self.assertEqual(response.status_code, 200)

    def test_template_usage(self):
        response = self.client.get('/accounts/verification/')
        self.assertTemplateUsed(response, 'account/verification.html')


class RegisterFormTest(TestCase):

    def test_form_fields(self):
        form = MainSignupForm()
        self.assertEqual(
            set(form.fields.keys()),
            {'username', 'password1', 'phone', 'email'}
        )
        self.assertIsInstance(form.fields['email'].widget, HiddenInput)
