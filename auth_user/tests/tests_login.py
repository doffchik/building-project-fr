import logging
from django.test import TestCase

logger = logging.getLogger(__name__)


class LoginTest(TestCase):

    def test_get_request(self):
        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    def test_template_usage(self):
        response = self.client.get('/accounts/login/')
        self.assertTemplateUsed(response, 'account/login.html')