from rest_framework import serializers
from django.contrib.auth import get_user_model


class MessageSerializers(serializers.ListSerializer):
    list = serializers.ListField(max_length=5)


User = get_user_model()

class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
            'first_name'
        ]
