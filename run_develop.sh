#!/bin/bash

# activate venv
cd ~ && cd my-projects/building-project-fr && source venv/bin/activate

(python manage.py runserver; docker run -p 6379:6379 redis; celery -A config worker -l info) | parallel

