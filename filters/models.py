import logging
from django.db import models
from django.db.models import Q, F, Avg
from django.conf import settings
from core.exceptions import FilterMaximumCountError
from . import decorators
from advertisement.models import ADS_TYPES, OBJECT_TYPES, Advertisement as Adv

logger = logging.getLogger(__name__)


@decorators.handle_db_error_for_get_choices()
def get_choices(field_name, default_option=None):
    # fields = [
    #     (field, field)
    #     for field in Adv.objects.values_list(field_name, flat=True).distinct()
    # ]
    fields = [('', '')]
    if default_option:
        fields.insert(0, ('', default_option))
    return fields


class Filter(models.Model):
    OBJECT_TYPES = OBJECT_TYPES.copy()
    OBJECT_TYPES.insert(0, ('', '"Тип нерухомості"'))

    ADS_TYPES = ADS_TYPES.copy()
    ADS_TYPES.insert(0, ('', '"Продаж/Оренда"'))

    """ Standard fields """

    object_type = models.CharField('Тип нерухомості', choices=OBJECT_TYPES, max_length=100, blank=True, null=True)
    adv_type = models.CharField('Тип оголошення', choices=ADS_TYPES, max_length=100, blank=True, null=True)

    settlement = models.CharField('Населений пункт', choices=get_choices('city'), max_length=255, blank=True)
    region = models.CharField('Область', choices=get_choices('region'), max_length=255, blank=True, null=True)
    district = models.CharField('Район', choices=get_choices('district', default_option='"Оберіть район"'),
                                max_length=255, blank=True, null=True)
    street = models.CharField('Вулиця', choices=get_choices('street', default_option='"Оберіть вулицю"'),
                              max_length=255, blank=True, null=True)

    price_start = models.FloatField('Ціна від', blank=True, null=True)
    price_end = models.FloatField('Ціна до', blank=True, null=True)
    currency = models.CharField('Валюта', choices=get_choices('currency'), max_length=50, blank=True, null=True)

    """ Additional fields """

    square_start = models.FloatField('Площа від', blank=True, null=True)
    square_end = models.FloatField('Площа до', blank=True, null=True)
    price_square_meter = models.FloatField('Ціна за кв/м', blank=True, null=True)

    # flats, offices, warehouses(складські приміщення)
    floor_start = models.IntegerField('Поверх від', blank=True, null=True)
    floor_end = models.IntegerField('Поверх до', blank=True, null=True)
    # -----------------------------------------------

    count_rooms = models.IntegerField('Кількість кімнат', blank=True, null=True)

    # plots(земельні ділянки)
    object_purpose = models.CharField('Цільове призначення', max_length=255, blank=True, null=True)
    # ----------------------

    # houses and flats
    building_type = models.CharField('Тип будинку', max_length=255, blank=True, null=True)
    building_status = models.CharField('Стан будинку', max_length=255, blank=True, null=True)
    # ----------------

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=True, related_name='filters')
    date_creation = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def save(self, *args, **kwargs):
        if self.filled_fields:
            super().save(*args, **kwargs)

    @property
    def all_fields(self):
        return ['object_type', 'adv_type', 'settlement', 'region', 'district',
                'street', 'price_start', 'price_end', 'currency']

    @property
    def filled_fields(self):
        return {
            attr: self.__dict__[attr]
            for attr in self.all_fields if self.__dict__[attr]
        }

    def save(self, *args, **kwargs):
        if self.user:
            if self.user.filters.count() > 2:
                raise FilterMaximumCountError('Current user have 3 filters. He can have more than 3')
        return super().save(*args, **kwargs)


    def query_condition(self):
        query_condition = Q()

        query_condition &= Q(obj_type=self.object_type) if self.object_type else Q()
        query_condition &= Q(adv_type=self.adv_type) if self.adv_type else Q()
        query_condition &= Q(city__contains=self.settlement) if self.settlement else Q()
        query_condition &= Q(region__contains=self.region) if self.region else Q()
        query_condition &= Q(district__contains=self.district) if self.district else Q()
        query_condition &= Q(street__contains=self.street) if self.street else Q()
        query_condition &= Q(price__gte=self.price_start) if self.price_start else Q()
        query_condition &= Q(price__lte=self.price_end) if self.price_end else Q()
        query_condition &= Q(currency=self.currency) if self.currency else Q()

        return query_condition

    def set_user(self, user):
        self.user = user
        self.save()

    def get_ads_query(self):
        qs = Adv.objects.filter(self.query_condition())

        # ToDo Fixe square type
        # qs = qs.annotate(price_square_meter=F('price')/F('square'))
        return qs

    @staticmethod
    def get_avg_price_for_adv(obj_type=None, adv_type=None, currency=None):
        if obj_type is None and adv_type is None:
            return None
        qs = Adv.objects.filter(
            Q(obj_type=obj_type) if obj_type else Q() &
            Q(adv_type=adv_type) if adv_type else Q() &
            Q(currency=currency))
        qs = qs.aggregate(avg_price=Avg(F('price')/F('square')))
        return qs

    class Meta:
        db_table = 'Filters'
        get_latest_by = 'date_creation'

    def __repr__(self):
        return f'filter-{self.object_type}-{self.id}'
