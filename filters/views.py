import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import reverse, get_object_or_404, render

from . import services, decorators
from .models import Filter
from advertisement.models import Advertisement

logger = logging.getLogger(__name__)


class AdsByFilter(services.AdsByFilterMixin):
    template_name = 'filters/ads_by_filter.html'
    search_by_query = True
    filter = None

    def get(self, request, filter_id):
        self.filter = Filter.objects.get(id=filter_id)
        self.context.update({'filter_form': self.filter_form(instance=self.filter),
                             'filter': self.filter})
        return super().get(request)

    def post(self, request, filter_id):
        self.filter = Filter.objects.get(id=filter_id)
        form = self.filter_form(request.POST, instance=self.filter)
        if form.is_valid():
            self.filter = form.save()
            return HttpResponseRedirect(reverse('filters:ads_by_filter', kwargs={'filter_id': self.filter.id}))
        return render(request, self.template_name, self.context)

    def set_query(self, request):
        self.query = self.filter.get_ads_query()


class AddFilter(LoginRequiredMixin, services.AdsByFilterMixin):
    template_name = 'filters/add_filter.html'

    @decorators.add_filter_permissions
    def get(self, request):
        self.context.update({'filter_form': self.filter_form})
        return super().get(request)

    @decorators.add_filter_permissions
    def post(self, request):
        form = self.filter_form(request.POST)

        if form.is_valid():
            filter = form.save()
            filter.set_user(request.user)
            return HttpResponseRedirect(reverse('filters:ads_by_filter', kwargs={'filter_id': filter.id}))
        return render(request, self.template_name, self.context)


def delete_filter(request, filter_id):
    filter = get_object_or_404(Filter, id=filter_id)
    filter.delete()
    return HttpResponseRedirect(reverse('advertisement:adv_list'))


def get_avg_price_for_square_meter_ajax(request, obj_type=None, adv_type=None, currency=None):
    json_data = Filter.get_avg_price_for_adv()
    return JsonResponse(json_data, status=200)


