import logging
from . import forms
from advertisement.services import AdvListMixin
from .forms import FilterForm


logger = logging.getLogger(__name__)


class AdsByFilterMixin(AdvListMixin):
    template_name = None
    filter_form = FilterForm


class FilterFormController:

    def __init__(self, _post_data):
        self._form_class = None
        self._data = _post_data

        self._set_form_class()

    def _set_form_class(self):

        if self._data.get('object_type'):
            self._form_class = forms.FilterForm

    def get_form_class(self):
        return self._form_class
