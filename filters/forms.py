import logging

from django import forms
from .models import Filter

logger = logging.getLogger(__name__)


class NameSetterModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._set_fields_class_name()

    def _set_fields_class_name(self):
        for key, value in self.fields.items():
            self.fields[key].widget.attrs['class'] = 'form-control'


class FilterForm(NameSetterModelForm):

    class Meta:
        model = Filter
        exclude = ['user', 'id', 'date_creation']
        widgets = {
            'price_start': forms.TextInput(attrs={'placeholder': 'Ціна Від'}),
            'price_end': forms.TextInput(attrs={'placeholder': 'Ціна До'}),

            'square_start': forms.TextInput(attrs={'placeholder': 'Площа Від'}),
            'square_end': forms.TextInput(attrs={'placeholder': 'Площа До'}),
            'price_square_meter': forms.TextInput(attrs={'placeholder': 'Ціна за кв/м'}),

            'floor_start': forms.TextInput(attrs={'placeholder': 'Поверх Від'}),
            'floor_end': forms.TextInput(attrs={'placeholder': 'Поверх До'}),
            'count_rooms': '',

            'object_purpose': forms.TextInput(attrs={'placeholder': 'Цільове призначення'}),
            'building_type': forms.TextInput(attrs={'placeholder': 'Тип будинку'}),
            'building_status': forms.TextInput(attrs={'placeholder': 'Стан об`єкту'}),
        }
