from django.urls import path
from . import views

app_name = 'filters'


urlpatterns = [
    path('add-filters/', views.AddFilter.as_view(), name='add_filter'),
    path('delete-filter/<int:filter_id>', views.delete_filter, name='delete_filter'),

    path('ads-by-filter/<int:filter_id>', views.AdsByFilter.as_view(), name='ads_by_filter'),
    path('get_adv_price-ajax/<str:obj_type>-<str:adv_type>-<str:currency>', views.get_avg_price_for_square_meter_ajax,
         name='get_avg_price_for_square_meter_ajax')
]
