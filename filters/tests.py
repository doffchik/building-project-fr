import logging
from pprint import pprint
import json

from django.test import TestCase, RequestFactory

from advertisement.models import Advertisement as Adv
from user.models import User
from core.exceptions import FilterMaximumCountError
from . import tools
from .views import AddFilter
from .models import Filter

logger = logging.getLogger(__name__)


class FilterMenuTest(TestCase):

    def test_button_with_link_to_filter_form(self):
        """ test: here we check if home page have button for creating filters"""
        response = self.client.get('/ads/')
        self.assertEqual(response.status_code, 200)
        text = response.content.decode('utf-8')
        self.assertIn('id_go_to_filter_form', text)


class FilterFormPageTest(TestCase):

    def setUp(self) -> None:
        self.user = User.objects.create_user(
            username='filter_tester',
            password='filter_tester',
        )

    def test_page_with_filter_form_for_not_authorized(self):
        """ test: if we can open page with filter form anonymous user """
        response = self.client.get('/add-filters/')
        self.assertRedirects(response, '/accounts/login/?next=/add-filters/')

    def test_page_with_filter_form_for_authorized(self):
        """ test: can open with authorized user """
        factory = RequestFactory()
        request = factory.get('')
        request.user = self.user
        response = AddFilter.as_view()(request)

        self.assertEqual(response.status_code, 200)


class FilterModelTest(TestCase):

    def create_ads(self):
        ads_data = json.loads(open('data/ads_data_for_filter_test.json').read())

        return [
            Adv.objects.create(**adv_properties)
            for adv_properties in ads_data
        ]

    def create_filters(self):
        """ method: create a filters for testing """
        filter1 = Filter.objects.create(
            object_type='Будинки',
            adv_type='Продаж',
        )

        filter2 = Filter.objects.create(
            price_start=4000,
            price_end=10000,
        )

        filter3 = Filter.objects.create(
            adv_type='Оренда',
            price_start=4000,
            price_end=10000,
            currency='грн'
        )
        filter4 = Filter.objects.create(
            adv_type='Продаж',
            object_type='Будинки',
            price_start=20000,
            price_end=60000,
            currency='$',
            settlement="Львів",
            region="Львівська",
            district="Сихівський",
            street="Кавалерідзе",
        )
        return filter1, filter2, filter3, filter4

    def test_create_filter(self):
        created_filter = Filter.objects.create(
            object_type='Квартира', adv_type='Продаж', settlement='Львів',
            region='Львівська', district='Франківський', street='Сахарова',
            price_start=3000, price_end=12000, currency='грн')

        self.assertEqual(Filter.objects.count(), 1)

        selected_filter = Filter.objects.order_by().first()

        self.assertEqual(selected_filter, created_filter)
        self.assertEqual(selected_filter.object_type, 'Квартира')
        self.assertEqual(selected_filter.adv_type, 'Продаж')
        self.assertEqual(selected_filter.currency, 'грн')
        self.assertEqual(selected_filter.price_start, 3000)
        self.assertEqual(selected_filter.price_end, 12000)

    def test_create_few_filters_for_one_user(self):
        user = User.objects.create_user(username='filter_tester', password='filter_tester')
        filter1 = Filter.objects.create(settlement='City 1')
        filter2 = Filter.objects.create(settlement='City 2')
        filter3 = Filter.objects.create(settlement='City 3')
        filter4 = Filter.objects.create(settlement='City 4')

        filter1.user = user
        filter2.user = user
        filter3.user = user
        filter4.user = user

        filter1.save(), filter2.save(), filter3.save()
        self.assertRaises(FilterMaximumCountError, filter4.save)

        self.assertEqual(
            [filter_.id for filter_ in user.filters.all()], [filter1.id, filter2.id, filter3.id]
        )

    def test_filter_condition(self):
        """ test: in this test we check if filter generate correct condition for filtering"""
        condition1 = {'adv_type': 'Оренда', 'price_start': 5000, 'price_end': 15000, 'currency': 'грн'}
        condition2 = {'object_type': 'Будинок', 'adv_type': 'Продаж', 'settlement': 'Львів', 'region': 'Львівська',
                      'district': 'Франківський', 'street': 'Лукаша'}
        condition3 = {
            'object_type': 'Земля', 'adv_type': 'Продаж', 'settlement': 'Самбір', 'region': 'Львівська',
            'district': 'Самбірський', 'street': 'Коперника', 'price_start': 3000, 'price_end': 12000,
            'currency': '$'}

        # here we create filters and for each filter and we rename fields names leave condition
        filters_with_conditions = [
            (Filter.objects.create(**cond), tools.change_filter_fields_names(cond))
            for num, cond in enumerate([condition1, condition2, condition3])
        ]

        def check_generation_condition_for_loyalty(filter__, condition):
            """ function: here we check if condition in filter is generated well """

            created_cond = filter__.query_condition().deconstruct()[1]
            org_cond_fields = set(condition.keys())

            # here we check if all filters field are some to renamed condition
            self.assertEqual(
                org_cond_fields,
                set(map(lambda x: x[0], created_cond))
            )

            for f_cond in created_cond:
                # check if created condition have some values as first condition
                self.assertIn(f_cond[0], org_cond_fields)
                self.assertEqual(f_cond[1], condition[f_cond[0]])

        for filter_, cond in filters_with_conditions:
            check_generation_condition_for_loyalty(filter_, cond)

    def test_ads_result_after_filtering(self):
        """ test: create few ads and than create few filters and test the result of filtering """

        ads = self.create_ads()
        self.assertEqual(Adv.objects.count(), len(ads))

        # in method create_filters we create few filter with different conditions
        filter1, filter2, filter3, filter4 = self.create_filters()

        self.assertEqual(filter1.get_ads_query().count(), 1)
        self.assertEqual(filter2.get_ads_query().count(), 3)
        self.assertEqual(filter3.get_ads_query().count(), 3)
        self.assertEqual(filter4.get_ads_query().count(), 1)


class FilterCreateForAuthorizedUserTest(TestCase):

    def test_create_filter_by_POST_request(self):
        pass

    def test_redirect_after_create_filter(self):
        pass
