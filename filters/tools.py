def change_filter_fields_names(condition: dict):

    new_condition = condition.copy()

    def update_field_name(old_name, new_name, value_):
        del new_condition[old_name]
        new_condition[new_name] = value_

    for field, value in condition.items():
        if field == 'settlement':
            update_field_name('settlement', 'city__contains', value)

        elif field == 'region':
            update_field_name('region', 'region__contains', value)

        elif field == 'district':
            update_field_name('district', 'district__contains', value)

        elif field == 'street':
            update_field_name('street', 'street__contains', value)

        elif field == 'price_start':
            update_field_name('price_start', 'price__gte', value)

        elif field == 'price_end':
            update_field_name('price_end', 'price__lte', value)

        elif field == 'object_type':
            update_field_name('object_type', 'obj_type', value)

    return new_condition
