import logging
from django.http import HttpResponse
from django.db.utils import ProgrammingError

logger = logging.getLogger(__name__)


filter_error_messages = "<h1>Ви не можете створити більше 3-ох фільтрів</h1>" \
                        "<h1>Рекомендація: Видаліть старі фільтри і створіть нові!</h1>"

django_initializing_new_db_warning = """
Warning at function {func.__name__}!\n
It is possible if you initializing db (Advertisement db is empty), but if not it is bad
"""


def add_filter_permissions(view_method):
    def wrapper(instance, request, *args, **kwargs):
        if request.user.filters.count() > 2:
            return HttpResponse(filter_error_messages, status=403)
        response = view_method(instance, request, *args, **kwargs)
        return response
    return wrapper


def handle_db_error_for_get_choices(show_logs=True):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                func(*args, **kwargs)
            except ProgrammingError:
                if show_logs:
                    logger.warning(django_initializing_new_db_warning.format(func=func))
        return wrapper
    return decorator
