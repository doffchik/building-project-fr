import logging


logger = logging.getLogger(__name__)


def template_setter(view_method):
    """ This decorator is used for moderator/customer system
    We must to understand with which templates we must work for rendering pages"""

    def wrapper(instance, request, *args, **kwargs):
        if not request.user.is_authenticated:
            instance.template_name = 'advertisement/AdvForClient.html'
        elif request.user.is_moderator:
            instance.template_name = 'advertisement/AdvForModerator.html'
        else:
            instance.template_name = 'advertisement/AdvForClient.html'

        return view_method(instance, request, *args, **kwargs)

    return wrapper
