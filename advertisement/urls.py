from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views

app_name='advertisement'

urlpatterns = [
    path('ads/', views.AdvList.as_view(), name='adv_list'),
    path('M-ads/', views.AdvListForModerator.as_view(), name='adv_list_for_moderators'),

    path('add/adv/', views.AdvAdd.as_view(), name='add_adv'),
    path('editing/<str:url_identifier>/', views.AdvUpdate.as_view(), name='update_adv'),

    path('adv/<str:url_identifier>/', views.AdvDetail.as_view(), name='adv_detail'),

    path('object/<str:type_>/', views.adv_by_type, name='adv_by_obj_type'),

    path('adv/<int:id>/archiving/', views.add_adv_to_archive, name='add_adv_to_archive'),

    path('archive/', views.Archive.as_view(), name='archive'),

    path('get-phone-ajax', views.get_adv_phone_ajax, name='get_adv_phone_ajax'),

    path('load-values-ajax/', views.load_values_ajax, name='load_values_ajax')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
