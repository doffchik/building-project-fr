import logging
from random import choice
from typing import Dict

from django.shortcuts import render, reverse
from django.views import View
from django.core.paginator import Paginator
from django.conf import settings
from django.db.models import Q, QuerySet

from .models import Advertisement, AdvImage


ADV_TYPES = {'flat': 'Кваритири', 'shop': 'Магазини', 'house': 'будинки', 'office': 'Офіси'}

logger = logging.getLogger(__name__)


class AdsPaginator:
    """
    AIM: this class works to get a list of ads from database

    DESCRIBE: class take a some parameter for filtering data or take ads query
              and than make a paginating.
              All this things is doing __init__.
              self.get_context - method to get dict with properties,
                                 which are result of paginating
    """

    def __init__(self, page_number, search_criteria=None, is_archival=False,
                 order_by=None, user=None, query=None):
        """for initialization we have a lot of parameters
        main parameters it is page_number.
        important parameter is 'query' if we will set it, class will not filter and sort ads by it self
        it will take advertisement from this query and do all paginating
        :parameter page_number     - is required. It is number of current page
        :parameter search_criteria - is not required. It is used for searching ads
                                     the type of this parameter is text,
                                     you can see the field for searching in method
                                     search_criteria_filter

        :parameter is_archival     - is not required. It is used for searching archived ads
                                     (adv.is_archival == True)

        :user           -

        :query                     - this parameter mean that we use some query of ads
                                     and we want to make paginating with them
        """

        self.query = query

        self._page_number = page_number
        self._search_criteria = search_criteria.strip() if isinstance(search_criteria, str) else search_criteria
        self._search_archival = is_archival
        self._order_by = order_by
        self._user = user

        self.paginator = None
        self.page_object = None
        self.ads_list = None
        self.is_paginated = None
        self.next_url = None
        self.prev_url = None
        self.last_page_number = None

        self._set_ads_list()
        self._to_paginate()
        self._set_navigation()

    def _set_ads_list(self):
        if self.query is None:
            self.ads_list = self._get_ads_list()
        else:
            self.ads_list = self.query

    def _get_ads_list(self):
        if self._user.is_authenticated:
            ads_list = Advertisement.objects.filter(
                is_archival=self._search_archival).filter(self.search_criteria_filter).exclude(
                id__in=self._user.hidden_ads.values_list('id', flat=True).distinct()
            )
        else:
            ads_list = Advertisement.objects.filter(
                self.search_criteria_filter
            ).filter(is_archival=self._search_archival)

        if self._order_by:
            ads_list = ads_list.order_by(self._order_by)

        return ads_list

    def _to_paginate(self):
        """
        Method get the number of page and make paginator for this
        If instance of this class have attr query it will paginate it, but if attr query will be None
        class will filter make filtering it self

        self._page_number - current page. Is used for detecting current navigation
        """

        self.paginator = Paginator(self.ads_list, 7)
        self.page_object = self.paginator.get_page(self._page_number)
        self.ads_list = self.page_object.object_list

    def _set_navigation(self):
        """
        method set navigation for current page
        we detect next, previous pages
        """

        self.is_paginated = self.page_object.has_other_pages()

        if self.page_object.has_next():
            self.next_url = f'?page={self.page_object.next_page_number()}'
        else:
            self.next_url = ''

        if self.page_object.has_previous():
            self.previous_url = f'?page={self.page_object.previous_page_number()}'
        else:
            self.previous_url = ''

        self.last_page_number = self.paginator.page_range[-1]

    @property
    def search_criteria_filter(self):
        filter = Q()

        if self._search_criteria:
            filter |= Q(obj_type=self._search_criteria)
            filter |= Q(title__contains=self._search_criteria)
            filter |= Q(new_title__contains=self._search_criteria)
            filter |= Q(description__contains=self._search_criteria)
            filter |= Q(city__contains=self._search_criteria)
            filter |= Q(street__contains=self._search_criteria)

        return filter

    def get_context(self):
        return {'ads_list': self.ads_list, 'page_object': self.page_object, 'is_paginated': self.is_paginated,
                'next_url': self.next_url, 'previous_url': self.previous_url, 'last_page':self.last_page_number,
                'current_page': self._page_number}


class AdvListMixin(View):

    template_name = None
    search_archival = False
    order_by = None
    search_by_query = None
    query = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.context = {}

    def get(self, request):

        user_ads_ids = None

        ads_page_list = self._get_ads(request)
        self.context.update(ads_page_list.get_context())

        if request.user.is_authenticated:
            user_ads_ids = get_user_ads_id(request.user)

        self.context.update({'MEDIA_URL': settings.MEDIA_ROOT, 'advertisement_id': user_ads_ids,
                        'types': ADV_TYPES})
        return render(request, self.template_name, context=self.context)

    def set_query(self, request):
        raise NotImplementedError('If you want filter by query, please extend method "set_query"')

    def _get_ads(self, request):
        if self.search_by_query:
            self.set_query(request)
            return AdsPaginator(request.GET.get('page', 1), query=self.query, user=request.user)
        else:
            return AdsPaginator(request.GET.get('page', 1), search_criteria=request.GET.get('search', ''),
                                is_archival=self.search_archival, order_by=self.order_by, user=request.user)

    @staticmethod
    def update_context(context:Dict, new_context: Dict):
        context.update(new_context)
        return context


def get_random_adv(current_adv_id):
    """ get random not archived ads from db """
    return Advertisement.objects.filter(is_archival=False).exclude(
        id=current_adv_id).order_by('?')[:4]


def get_advertisements_by_obj_type(obj_type):
    advertisements = Advertisement.objects.filter(obj_type=obj_type)
    return advertisements


def get_user_ads_id(user):
    return [adv.id for adv in user.added_ads.all()]


class Archiver:
    """ class is used for archiving ads
    after archiving you can use method "get_end_point_for_redirect"
    it return new url for redirect """

    def __init__(self, id):
        self.adv = Advertisement.objects.get(id=id)
        self._add_adv_to_archive()

    def _add_adv_to_archive(self):
        self.adv.add_to_archive()

    @staticmethod
    def get_end_point_for_redirect(full_url_path):
        """method should take full url path from url which call this view.
        it detect were it should be redirected to, return url """

        for point in full_url_path.split('/'):
            if point == 'adv':
                return reverse('advertisement:archive')

            elif point == 'M-advertisements':
                return reverse('advertisement:adv_list_for_moderators')

            elif point == 'advertisements':
                return reverse('advertisement:adv_list')


def store_adv_images(adv, images):
    adv = Advertisement.objects.get(id = adv.id)
    return [AdvImage(adv=adv, image=file).save() for file in images]
