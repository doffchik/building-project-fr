from django import forms

from .models import Advertisement, City, Region, District, Street
from filters.forms import NameSetterModelForm


class AdvUpdateForm(NameSetterModelForm):

    class Meta:
        model = Advertisement
        exclude = ('date_pub', 'title_slug', 'photo_links',
                   'seller_id', 'original_url')

class AdvAddForm(NameSetterModelForm):

    images = forms.ImageField(
        label='Зоображення',
        widget=forms.ClearableFileInput(attrs={'multiple':True})
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.fields['region'].initial = Region.objects.get(name='Львівська')
        self.fields['region'].queryset = Region.objects.all()
        # self.fields['city'].queryset = City.objects.none()
        # self.fields['district'].queryset = District.objects.none()
        # self.fields['street'].queryset = Street.objects.none()

    class Meta:
        model = Advertisement
        exclude = ('date_pub', 'title_slug', 'photo_links',
                   'seller_id', 'original_url')
