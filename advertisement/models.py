import json
import os
from datetime import datetime
from translitua import translit

from django.db import models
from django.shortcuts import reverse
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from .tools import store_serialized_data

OBJECT_TYPES = [
    ('flats', 'Квартири'),
    ('houses', 'Будинки'),
    ('plots', 'Земельні ділянки'),
    ('shops', 'Магазини'),
    ('warehouses', 'Складські приміщення'),
    ('offices', 'Офіси'),
    ('individual_buildings', 'Окремі будівлі')
]
ADS_TYPES = [
    ('Продаж', 'Продаж'),
    ('Оренда', 'Оренда')
]

CURRENCIES = [
    ('dollar', 'долар'),
    ('euro', 'євро'),
    ('grn', 'грн'),
]


class Region(models.Model):

    name = models.CharField(_('Область'), max_length=255, db_index=True)

    def __str__(self):
        return f'обл. {self.name}'

    class Meta:
        db_table = 'Region'


class City(models.Model):

    name = models.CharField(_("Місто"), max_length=255, db_index=True)
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, null=True, related_name='cities', db_index=True)

    def __str__(self):
        return f'м. {self.name}'

    class Meta:
        db_table = 'City'


class District(models.Model):

    name = models.CharField(_('Район'), max_length=255, db_index=True)

    region = models.ForeignKey(Region, null=True, on_delete=models.SET_NULL, related_name='districts', db_index=True)
    city = models.ForeignKey(City, null=True, on_delete=models.SET_NULL, related_name='districts', db_index=True)

    is_city_district = models.BooleanField(default=False, blank=True, null=True)

    def __str__(self):
        return f'р. {self.name}'

    class Meta:
        db_table = 'District'


class Street(models.Model):

    name = models.CharField('Вулиця', max_length=255, db_index=True)

    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, related_name='streets', db_index=True)
    district = models.ForeignKey(District, on_delete=models.SET_NULL, null=True, related_name='streets', db_index=True)

    def __str__(self):
        return f'вул. {self.name}'

    class Meta:
        db_table = 'Street'


class Advertisement(models.Model):

    date_pub = models.DateTimeField(_("Date of publication"), auto_now_add=True, null=True)

    region = models.ForeignKey(Region, on_delete=models.SET_NULL, related_name='ads', db_index=True, null=True)
    city = models.ForeignKey(City, on_delete=models.SET_NULL, related_name='ads', db_index=True, null=True)
    district = models.ForeignKey(District, on_delete=models.SET_NULL, related_name='ads', db_index=True, null=True)
    street = models.ForeignKey(Street, on_delete=models.SET_NULL, related_name='ads', db_index=True, null=True)

    title = models.CharField(_("Назва оголошення"), max_length=250, db_index=True)
    title_slug = models.SlugField(max_length=250, blank=True, null=True, allow_unicode=True)
    new_title = models.CharField(max_length=250, db_index=True,blank=True, null=True)
    description = models.TextField(_("Опис"), blank=True, null=True)
    gen_desc = models.TextField(blank=True, null=True)

    currency = models.CharField(_("Валюта"), max_length=255,choices=CURRENCIES, blank=True, null=True)
    price = models.FloatField(_("Ціна"), max_length=255, blank=True, null=True)

    adv_type = models.CharField(_("Тип оголошення"),choices=ADS_TYPES,  max_length=255, blank=True, null=True)
    obj_type = models.CharField(choices=OBJECT_TYPES, blank=True, null=True, max_length=100)
    building = models.CharField(_("Будинок"), max_length=255, blank=True, null=True)

    photo_links = models.TextField(_("Посилання на фотографії"), blank=True, null=True)

    kitchen = models.CharField(_("Кухня"), max_length=255, blank=True, null=True)
    floor = models.CharField(_("Поверх"), max_length=255, blank=True, null=True)
    bathroom = models.CharField(_("Ванна"), max_length=255, blank=True, null=True)
    walls = models.CharField(_("Стіни"), blank=True, null=True, max_length=255)

    square = models.CharField(_("Площа"), max_length=255, blank=True, null=True)
    count_floors = models.CharField(_("Кількість поверхів"), max_length=255, blank=True, null=True)
    rooms = models.IntegerField(_("Кількість кімнат"), blank=True, null=True)

    heating = models.CharField(_("Опалення"), max_length=255, blank=True, null=True)
    remont = models.CharField(_("Ремонт"), max_length=255, blank=True, null=True, )
    planing = models.CharField(_("Планування"), max_length=255, blank=True, null=True, )

    seller_id = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(_("Номер телефону"), max_length=255, blank=True, null=True)
    original_url = models.CharField(max_length=255, blank=True, null=True)

    is_archival = models.BooleanField(null=False, default=False)
    date_archival = models.DateTimeField(default=None, null=True, blank=True)

    url_identifier = models.SlugField(max_length=255, null=True, unique=True, db_column='unique_adv_url_identifier',
                                       blank=True)

    def __getattribute__(self, item):
        if item == 'url_identifier':
            if not super().__getattribute__(item):
                self._set_url_identifier()
            return super().__getattribute__(item)
        else:
            return super().__getattribute__(item)

    def _set_url_identifier(self):
        self.url_identifier = slugify(translit(
            f'{"".join([i for i in self.region.name if i.isalnum()]) if self.region else "" }-'
            f'{"".join([i for i in self.street.name if i.isalnum()]) if self.street else "" }'
            f'-{Advertisement.objects.latest("id").id + 1}'
        ))


    def __str__(self):
        return f'<<{self.__class__.__name__} (id={self.id})>>'

    def save(self, *args, **kwargs):
        self.title_slug = slugify(self.title, allow_unicode=True)
        super(Advertisement, self).save(*args, **kwargs)

    def get_small_description(self, count=50):
        words = self.gen_desc.split(' ')
        if len(words) <= 50:
            return ' '.join(words)
        return ' '.join(words) + '...'

    def get_small_title(self):
        if self.new_title and len(self.new_title) > 35:
            return self.new_title[:32] + '...'
        return self.new_title

    def get_main_page_url(self):
        return reverse('advertisement:advertisement_list_for_customers')

    def get_absolute_url(self):
        return reverse('advertisement:adv_detail', kwargs={'url_identifier':self.url_identifier})

    def get_reviewing_images_url(self):
        return reverse('advertisement:reviewing_images_url', kwargs={'id': self.id})

    def get_test_phone(self):
        if not self.phone:
            return '+38-067-456-1233'
        return self.phone

    def get_test_date(self):
        if not self.date_pub:
            from django.utils import timezone
            return timezone.now()
        return self.date_pub

    def len_gen_title(self):
        if self.new_title:
            return len(self.new_title)

    def add_to_archive(self):
        self.is_archival = True
        self.date_archival = timezone.now()
        self.save()

    def get_title(self):
        if self.new_title and len(self.new_title) > 10:
            return self.new_title
        return self.title

    @classmethod
    def get_phone(cls, id):
        adv = cls.objects.get(id=id)
        if adv.phone:
            return adv.phone
        else:
            return '+38-096-234-1233'

    @classmethod
    def get_serialized_ads(cls, count=None, write=True):

        ads_properties = [
            adv.__dict__ for
            adv in cls.objects.order_by('?')[: count if count else cls.objects.count()]
        ]
        def _cleaning(data): del data['url_identifier']; del data['_state']; del data['id']

        [_cleaning(properties) for properties in ads_properties ]

        if write:
            store_serialized_data(ads_properties)
        return ads_properties

    @classmethod
    def create_from_file(cls, file=None, count='__all__'):
        ads_properties = json.load(open(file))

        count = len(ads_properties) if count == '__all__' else count
        ads = [
            cls.objects.create(**single_adv_property)
            for single_adv_property in ads_properties[:count]
        ]
        return ads

    @property
    def get_main_image(self):
        adv_images = self.images.all()
        return adv_images and adv_images[0].image.url

    get_images_count = property(lambda self: self.images.count())
    get_images = property(lambda self: [image.image.url for image in self.images.all()])

    get_full_address = property(lambda self: f'м. {self.city} \t вул. {self.street}')

    class Meta:
        db_table = 'Advertisement'
        ordering = ['-date_pub']

        verbose_name_plural = _('Advertisement')
        verbose_name = _('Advertisements')


def get_upload_path(instance, filename):
    filename = f"img_{instance.pk}_{filename.split('.')[-1]}"
    return os.path.join('ads_images', instance.adv.url_identifier, filename)


class AdvImage(models.Model):

    adv = models.ForeignKey(Advertisement, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to=get_upload_path)
    upload_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'AdvImage'
