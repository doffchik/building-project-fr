from django.contrib import admin
from .models import *


class AdvertisementAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'street', 'gen_desc', 'price', 'date_pub']
    list_display_links = ['id', 'title']
    search_fields = ['obj_type', 'street', 'title', 'price']

admin.site.register(Advertisement, AdvertisementAdmin)

admin.site.register(Region)
admin.site.register(City)
admin.site.register(District)
admin.site.register(Street)
