import json
import os
from datetime import datetime

from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder


def store_serialized_data(serialized_data):
    file_name = f'ads_{datetime.now().strftime("%b-%d-%Y(%H:%M:%S)")}.json'

    with open(os.path.join(settings.BASE_DIR, 'serialized_ads', file_name), 'a') as file:

        json_data = json.dumps(serialized_data, indent=4, ensure_ascii=False, cls=DjangoJSONEncoder)

        file.write(f'{json_data}')
