import logging
import unittest

from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase


class AdvertisementHomePage(StaticLiveServerTestCase):

    def setUp(self) -> None:
        self.browser = webdriver.Firefox()

    def tearDown(self) -> None:
        self.browser.quit()

    def test_template_usage(self):
        response = self.client.get('/ads/')
        self.assertTemplateUsed(response, 'advertisement/AdvList.html')



