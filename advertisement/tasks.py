import time
import logging
logger = logging.getLogger(__name__)

from config.celery_config import app


@app.task
def debug_task(mode=None):

    if mode == 'run':
        counter = 0
        while True:
            if counter > 25:
                break
            else:
                logger.info('It is test task')
                time.sleep(0.25)
                counter += 1