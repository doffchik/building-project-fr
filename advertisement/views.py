import logging

from django.views import generic
from django.shortcuts import render, HttpResponseRedirect, reverse
from django.views.generic import CreateView, UpdateView
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.views.generic.edit import FormView

from advertisement.models import Advertisement, Street, District, Region, City
from .forms import AdvUpdateForm, AdvAddForm
from . import services, decorators

from user import services as user_services
from site_messages.forms import MessageForm
from site_messages.services import MessageManager

logger = logging.getLogger(__name__)


class AdvList(services.AdvListMixin):
    template_name = 'advertisement/AdvList.html'

    def get(self, request):
        return super(AdvList, self).get(request)


class AdvListForModerator(services.AdvListMixin):
    template_name = 'advertisement/AdvListForModerator.html'


class AdvDetail(generic.DetailView):
    model = Advertisement

    slug_field = 'url_identifier'
    slug_url_kwarg = 'url_identifier'

    template_name = None  # I am using "template_setter" decorator for detecting template. Read why in doc
    context_object_name = 'advertisement'

    @decorators.template_setter
    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        user_services.UserAdv(adv=self.object, user=request.user).add_reviewed_adv_for_user()
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['random_adv'] = services.get_random_adv(current_adv_id=self.object.id)
        context['message_form'] = MessageForm()
        message_manager = MessageManager(self.request.GET.get('client_id'), self.object, self.request.user)
        context['messages'] = message_manager.get_messages()
        context['adv_clients'] = message_manager.clients_id if hasattr(message_manager, 'clients_id') else None
        return context

    @decorators.template_setter
    def post(self, request, url_identifier, *args, **kwargs):
        form = MessageForm(request.POST)

        if form.is_valid():
            manager = MessageManager(
                client_id=request.GET.get('client_id'),
                adv=Advertisement.objects.get( url_identifier= url_identifier), user=request.user
            )
            manager.send_messages(form.cleaned_data['text'])
            return HttpResponseRedirect(f'{request.get_full_path()}#messages-block')
        return render(request, self.template_name)


def adv_by_type(request, type_):
    ads_page_list = services.AdsPaginator(request.GET.get('page', 1), search_criteria=type_)
    context = ads_page_list.get_context()
    return render(request, 'advertisement/AdvList.html', context=context)


class AdvAdd(LoginRequiredMixin, CreateView):
    login_url = '/accounts/login/'

    template_name = 'advertisement/AdvertisementCreating.html'

    model = Advertisement
    form_class = AdvAddForm

    def post(self, request, *args, **kwargs):
        form = self.get_form(self.get_form_class())
        if form.is_valid():
            result = self.form_valid(form)
            services.store_adv_images(adv=self.object, images=request.FILES.getlist('images'))
            return result
        return render(request, self.template_name, {'form': form})


class AdvUpdate(PermissionRequiredMixin, UpdateView):

    permission_required = 'advertisement.change_advertisement'

    slug_field = 'url_identifier'
    slug_url_kwarg = 'url_identifier'

    template_name = 'advertisement/AdvUpdate.html'
    model = Advertisement
    form_class = AdvUpdateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['adv'] = self.object
        return context


def add_adv_to_archive(request, id):
    archiver = services.Archiver(id)
    return HttpResponseRedirect(archiver.get_end_point_for_redirect(request.GET.get('redirect_to')))


class Archive(services.AdvListMixin):
    template_name = 'advertisement/archive.html'
    order_by = '-date_archival'

    search_archival = True


def get_adv_phone_ajax(request):
    adv_id = request.POST.get('adv_id')

    phone = Advertisement.get_phone(adv_id)
    return JsonResponse({'adv_id': adv_id, 'button_id': request.POST.get('button_id'),
                         'is_authenticated': request.user.is_authenticated, 'phone': phone})


def load_values_ajax(request):
    field_changed = request.GET.get('field_changed')[1:]
    value_id = request.GET.get('value_id')

    if not value_id:
        return render(request, 'includes/dropdown_list_options.html')

    reg_id = request.GET.get('current_region')
    cit_id = request.GET.get('current_city')
    dist_id = request.GET.get('current_district')
    str_id = request.GET.get('current_street')

    current_region = Region.objects.get(id=reg_id) if reg_id else ''
    current_city = City.objects.get(id=cit_id) if cit_id else ''
    current_district = District.objects.get(id=dist_id) if dist_id else ''
    current_street = Street.objects.get(id=str_id) if str_id else ''

    if field_changed == 'id_region':
        regions = Region.objects.exclude(id=value_id)
        cities = City.objects.filter(region_id=value_id)
        districts = District.objects.filter(region_id=value_id, is_city_district=False)

    elif field_changed == 'id_city':
        streets = Street.objects.filter(city_id=value_id)
        districts = District.objects.filter(city_id=value_id)
        cities = City.objects.exclude(id=value_id)

        if len(districts) == 1:
            current_district = districts[0]
            districts = District.objects.filter(region_id=reg_id, is_city_district=False)
        else:
            current_district = None


    elif field_changed == 'id_district':
        current_city = current_district.city
        cities = City.objects.exclude(id=current_city.id)

        if current_district.is_city_district:
            districts = District.objects.filter(city=current_district.city, is_city_district=True)
        else:
            districts = District.objects.exclude(id=value_id, is_city_district=True)


    return render(request, 'includes/dropdown_list_options.html', locals())
