import logging

logger = logging.getLogger(__name__)


def clean_cookies(method):
    def wrapper(instance, *args, **kwargs):
        instance.browser.delete_all_cookies()
        return method(instance, *args, **kwargs)
    return wrapper


def authorize(instance, credentials='bot_tester'):

    instance.browser.get(f'{instance.live_server_url}/accounts/login/')

    instance.browser.find_element_by_id('id_login').send_keys(credentials)
    instance.browser.find_element_by_id('id_password').send_keys(credentials)

    instance.browser.find_element_by_id('id_login_submit').click()

    return instance.browser
