tag_id_name_and_placeholder_value = {
    'id_price_start': 'Ціна Від',
    'id_price_end': 'Ціна До',
    'id_square_start': 'Площа Від',
    'id_square_end': 'Площа До',
    'id_floor_start': 'Поверх Від',
    'id_floor_end': 'Поверх До',
    'id_object_purpose': 'Цільове призначення',
    'id_building_type': 'Тип будинку',
    'id_building_status': 'Стан об`єкту',
    'id_price_square_meter': 'Ціна за кв/м',
}