import logging

from . import tools
from .base import FunctionalTest

logger = logging.getLogger(__name__)


class TestMessageMenuButton(FunctionalTest):

    def test_button_in_header_for_authorized_user(self):
        # user open home page
        self.browser.get(self.live_server_url)

        # than user login
        self.browser = tools.authorize(self)

        # user see message menu button in header and than click
        self.browser.find_element_by_id('id_message_menu_button').click()

        # and than user see page with his messages
        self.assertEqual(self.browser.title, 'Мої повідомлення')
        self.browser.find_element_by_id('messages-block')


class TestMessageListPage(FunctionalTest):

    def test_client(self):
        """ test list of messages for client """
        self.browser.get(self.live_server_url + '/my-messages/')

        # user should see block with messages
        self.browser.find_element_by_id('messages-list')

    def test_moderator(self):
        """ test list messages for moderator """
        pass
