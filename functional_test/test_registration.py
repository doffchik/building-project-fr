import logging
from .base import FunctionalTest

logger = logging.getLogger(__name__)


class TestVisitorRegistration(FunctionalTest):

    def user_get_his_verification_code(self):
        # ToDo user should get code form viber
        pass

    def test_user_registers(self):
        # user open page with registration form
        self.browser.get(f'{self.live_server_url}/accounts/signup/')
        self.assertEqual(self.browser.title, 'Реєстрація')

        # user see field for phone,username and password and than user fill form register
        self.browser.find_element_by_id('id_password1').send_keys('bot_test_register_form')
        self.browser.find_element_by_id('id_username').send_keys('bot_test_register_form')
        self.browser.find_element_by_id('id_phone').send_keys('380966545888')
        self.browser.find_element_by_id('id_sing-up').click()

        # after registration user see verification page
        self.assertEqual(self.browser.title, 'Підтвердження особи')
        self.assertIn(
            'Підтвердіть ваш телефон',
            self.browser.find_element_by_id('id_verification_header').text
        )

        # than user check verification code
        code = self.user_get_his_verification_code()

        # user enter the code and than submit form
        self.browser.find_element_by_id('id_verification_code').send_keys(code)
        self.browser.find_element_by_id('id_submit_code').click()

        # user see home page and Profile button in header
        self.assertEqual(self.browser.title, 'Львівська Нерухомість')
        self.assertEqual(self.browser.find_element_by_id('id_user_profile').text, 'Профіль')

        self.fail('Test is finished')
    #
    # def test_user_verification_page(self):
    #
    #     # user open verification page
    #     self.browser.get(f'{self.live_server_url}/accounts/verification/')
    #
    #     # user see form for code
    #     self.browser.find_element_by_id('id_verification_code')
    #     self.browser.find_element_by_id('id_submit_code')
