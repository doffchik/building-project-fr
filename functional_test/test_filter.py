import time
import logging

from selenium import webdriver
from selenium.webdriver.support.ui import Select
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from . import tools
from . import constants

logger = logging.getLogger(__name__)


class FilterMenuTest(StaticLiveServerTestCase):

    def setUp(self) -> None:
        self.browser = webdriver.Firefox()

    def tearDown(self) -> None:
        self.browser.quit()

    def iterate_all_filters_list(self):
        filters_list = self.browser.find_element_by_class_name('filters')
        filters = list()

        for filter in filters_list.find_elements_by_class_name('filter'):
            filter.get_attribute('href').startswith('/ads-by-filter/')
            filters.append(filter)

        return filters

    def test_filter_menu(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)

        # test filter list for not authorized user
        self.iterate_all_filters_list()

        # now we test with authorized user
        self.browser = tools.authorize(self)

        # and now we should test if for authorized user
        filters = self.iterate_all_filters_list()
        self.assertLessEqual(len(filters), 3)

    def test_button_with_link_to_filter_form(self):
        """ test: in this test we check if button 'create-filter' is work good
        it should open page with filter form """

        # open browser window with home page and check if button is posted
        self.browser.get(self.live_server_url)
        button_to_form = self.browser.find_element_by_id('id_go_to_filter_form')

        # now we want to create a filter, let`s open the page with form
        button_to_form.click()

        # check if we are redirected to login page
        self.assertEqual('Вхід в акаунт', self.browser.title)
        self.assertIn('login', self.browser.current_url)

        # now we want try to login first and than crate filter
        ## We change our browser because we want to clean all cookie nad cache
        self.browser.quit()
        self.browser = webdriver.Firefox()
        self.browser.get(self.live_server_url)

        # now we want login to our web-site
        tools.authorize(self)

        # now let`s try to open filter form
        button_to_form = self.browser.find_element_by_id('id_go_to_filter_form')
        button_to_form.click()

        # check url and title of page with filter form
        self.assertIn('add-filter', self.browser.current_url)
        self.assertEqual('Створіть новий фільтр', self.browser.title)

        # check if form is displayed
        self.assertTrue(
            self.browser.find_element_by_id('filter-form'),
            msg='filter form block is not rendered form '
        )

        self.browser.find_element_by_id('create-filter')


class FilterFormPageTest(StaticLiveServerTestCase):

    def setUp(self) -> None:
        self.browser = webdriver.Firefox()

    def tearDown(self) -> None:
        self.browser.quit()

    def test_filter_form_page(self):
        """ test: we should check if filter form is rendered good """

        # first we should login
        self.browser = tools.authorize(self)

        self.browser.get(f'{self.live_server_url}/add-filters/')

        # check default values
        self.assertEqual(
            '"Тип нерухомості"', self.browser.find_element_by_id(
                'id_object_type').find_element_by_tag_name('option').text
        )
        self.assertEqual(
            '"Продаж/Оренда"', self.browser.find_element_by_id(
                'id_adv_type').find_element_by_tag_name('option').text
        )

        for tag_id, value in constants.tag_id_name_and_placeholder_value.items():
            self.assertEqual(
                self.browser.find_element_by_id(tag_id).get_attribute('placeholder'), value
            )


class FilterCreateTest(StaticLiveServerTestCase):

    def setUp(self) -> None:
        self.browser = webdriver.Firefox()

    def tearDown(self) -> None:
        self.browser.quit()

    def test_create_filter_by_form(self):

        # first we should authorized
        self.browser = tools.authorize(self)

        self.browser.get(self.live_server_url)
        filters = self.browser.find_elements_by_class_name('filter')
        count_filters_before_creating_new = len(filters)

        self.browser.get(f'{self.live_server_url}/add-filters/')
        time.sleep(0.5)
        # we want to select object type as Квартири
        selected_obj_type_field = Select(self.browser.find_element_by_id('id_object_type'))
        selected_obj_type_field.select_by_visible_text('Квартири')

        # now we set price of expected real estate
        self.browser.find_element_by_id('id_price_start').send_keys(5000)
        self.browser.find_element_by_id('id_price_end').send_keys(30000)

        # set currency for ads
        ## P.S for now it is just a file, but in future it should be drop-down list
        self.browser.find_element_by_id('id_currency').send_keys('$')
        self.browser.find_element_by_id('id_currency').send_keys('$')

        # now we set the middle price of expected ads
        self.browser.find_element_by_id('id_price_square_meter').send_keys(200)

        # now we should create filter
        ## we submit the filter form and send data, after this we will be redirected to home page
        self.browser.find_element_by_id('create-filter').click()
        time.sleep(15)
        self.assertEqual('Львівська нерухомість', self.browser.title)

        filters = self.browser.find_elements_by_class_name('filter')

        # now we should see if filer was created and added to a list in filters menu
        self.assertEqual(count_filters_before_creating_new, len(filters) + 1,
                         msg='Filter was not added')